# Pasta de Documentos #

Esta pasta sera usada para o armazenamento dos documentos do projeto.

### Pastas ###

####Entidade Relacionamento####
* Gliffy: Pasta para arquivos exportado do diagrama disponivel no site (https://www.gliffy.com)
* Image: Pasta de imagens do Diagrama Entidade Relacionamento

####Modelo Relacional####
* Image: Pasta para imagens do Modelo Relacional 
* Workbanch: Pasta para arquivos do Modelo Relacional executado no Workbanch

####Diagrama de Classes####
* Astah: Pasta para arquivos do Diagrama de Classes executado no Astah
* Image: Pasta para imagens do Diagrama de Classes
