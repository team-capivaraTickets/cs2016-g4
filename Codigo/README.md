# CapivaraTickets

> Projeto da matéria Construção de Software II

## Pré-requisitos
* [Postgres](http://www.postgresql.org/)
* [Node.JS](https://nodejs.org)

## Tecnologias utilizadas
**Back-end**

* [Sails](http://sailsjs.org/) - Usado como framework back-end para agilizar o processo de desenvolvimento.


**Front-end**

* [Angular](https://angularjs.org/) - Usado para fazer a experiência do usuário mais rica, com interação e eficiência.

* [Bootstrap](http://getbootstrap.com/) - Framework front-end para facilitar o desenvolvimento da interface com o usuário.


## Configuração do projeto
No arquivo `Codigo / config / connections.js` altere o bloco abaixo com as informações do seu ambiente:
```js
somePostgresqlServer: {
    adapter: 'sails-postgresql',
    host: 'localhost',
    user: 'user_name',
    password: 'user_password',
    database: 'database_name'
  }
```
## Como usar

É necessario ter instalado os pacotes `bower`, `sails`:
```shell
npm install -g bower
```
```shell
npm install -g sails
```

Faça download do projeto e execute os comandos na pasta `Codigo`, se estiver em ambiente Windows rode o terminal como administrador:
```shell
npm install
```

```shell
bower install
```

Após isto, para rodar o sistema execute `sails lift` ou `npm start`:
```shell
sails lift | npm start
```

## Estrutura do projeto

    ├── api
    │   ├── controllers     - Controladores para requisições
    │   ├── models          - Entidades do sistema
    │   ├── policies        - Regras aplicadas sobre as requisições
    │   ├── responses       - Respostas personalizadas para requisições
    │   ├── services        - Funções reutilizáveis
    │
    ├── assets              - Aquivos estáticos acessíveis pelo navegador
    │   ├── images          - Imagens usados no aplicativo
    │   ├── js              - Arquivos javascript feitos para o aplicativo
    │   │   ├── config      - Arquivos de configuração usados no angular
    │   │   ├── constant    - Informações estáticas usados na interface
    │   │   ├── controllers - Controladores do angular
    │   │   ├── directives  - Diretivas do angular
    │   │   ├── resources   - Entidades do sistema usados no angular
    │   │   └── services    - Arquivos com funções reutilizáveis usadas na interface
    │   │
    │   ├── styles          - Arquivos de estilo (css) do aplicativo
    │   └── templates       - Templates html usados pela visão do angular
    │
    ├── config              - Arquivos de configuração do Sails
    │
    ├── tasks               - Tarefas do Grunt
    │
    └── views               - Arquivos de visão processados no servidor
