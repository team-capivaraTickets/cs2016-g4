angular.module('app')
    .factory('Pessoa', function ($resource) {
        return $resource('/api/pessoa/:id/:controller', {
                id: '@id'
            },
            {
                changePassword: {
                    method: 'PUT',
                    params: {
                        controller: 'password'
                    }
                },
                get: {
                    method: 'GET',
                    params: {
                        id: 'me'
                    }
                },
                profile: {
                    method: 'GET',
                    params: {
                        id: '@id'
                    }
                },
                update: {
                    method: 'PUT',
                    params: {
                        id: '@id'
                    }
                }
            });
    });
