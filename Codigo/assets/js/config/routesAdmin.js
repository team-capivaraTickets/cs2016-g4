angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {

    $stateProvider
      .state('admin', {
        abstract: true,
        url: "/admin",
        templateUrl: "templates/admin/index.html"
      })
      .state('admin.home', {
        url: "",
        templateUrl: "templates/admin/dashboard.html",
        controller: "DashboardCtrl",
        data: {
          authorization: true,
          role: ['admin'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.evento', {
        url: "/Evento",
        templateUrl: "templates/prop/listarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.rascunho', {
        url: "/Evento/Rascunhos",
        templateUrl: "templates/prop/listarEventoRascunho.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.historico', {
        url: "/Evento/Historico",
        templateUrl: "templates/prop/listarEventoHistorico.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.criarEvento', {
        url: "/Evento/criarEvento",
        templateUrl: "templates/prop/criarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.visualizarEvento', {
        url: "/visualizarEvento/:id",
        templateUrl: "templates/prop/visualizarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.editarEvento', {
        url: "/Evento/editarEvento/:id",
        templateUrl: "templates/prop/editarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.agenda', {
        url: "/Agenda/:id",
        templateUrl: "templates/prop/listarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.criarAgenda', {
        url: "/Agenda/criarAgenda/:id",
        templateUrl: "templates/prop/criarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.editarAgenda', {
        url: "/Agenda/editarAgenda/:id?:idAgenda",
        templateUrl: "templates/prop/editarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.agendaHistorico', {
        url: "/Agenda/Historico/:id",
        templateUrl: "templates/prop/listarAgendaHistorico.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.ingresso', {
        url: "/Ingresso/:idAgenda",
        templateUrl: "templates/prop/listarIngresso.html",
        controller: "IngressoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.ingressoHistorico', {
        url: "/Ingresso/Historico/:idAgenda",
        templateUrl: "templates/prop/listarIngressoHistorico.html",
        controller: "IngressoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.editarIngresso', {
        url: "/editarIngresso/:idIngresso",
        templateUrl: "templates/prop/editarIngresso.html",
        controller: "IngressoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.relatorioDeVendas', {
        url: "/relatorio/vendas",
        templateUrl: "templates/prop/relatorioVendas.html",
        controller: "RelatorioCtrl",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.pagamento', {
        url: "/Pagamentos",
        templateUrl: "templates/admin/listarPagamento.html",
        controller: "CarrinhoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.editarPagamento', {
        url: "/Pagamentos/editarPagamento/:idPagamento",
        templateUrl: "templates/admin/editarPagamento.html",
        controller: "CarrinhoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('admin.pessoa', {
        url: "/Pessoa",
        templateUrl: "templates/main/consultarPessoa.html",
        controller: "PessoaCtrl"
      })
      .state('admin.enviarEmail', {
        url: "/EnviarEmail",
        templateUrl: "templates/admin/settings.html",
        controller: "SettingsCtrl",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      });

  }]);
