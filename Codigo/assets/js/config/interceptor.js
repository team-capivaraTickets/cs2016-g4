angular.module('app')
  /**
   * Cria um Injector para interceptar a requisição http e adicionar o token jwt
   */
  .factory('JwtInjector', [function () {
    var sessionInjector = {
      request: function (config) {
        if (localStorage.getItem('token')) {
          config.headers['Authorization'] = 'JWT ' + localStorage.getItem('token');
        }
        return config;
      }
    };
    return sessionInjector;
  }])
  /**
   * adiciona o Injector a configuração do modulo
   */
  .config(['$httpProvider', function ($httpProvider) {

    $httpProvider.interceptors.push('JwtInjector');

  }]);
