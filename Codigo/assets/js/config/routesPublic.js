angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {

    $stateProvider
      .state('main', {
        abstract: true,
        url: "",
        templateUrl: "templates/main/index.html"
      })
      .state('main.home', {
        url: "/",
        templateUrl: "templates/main/home.html",
        controller: "EventoController"
      })
      .state('main.profile', {
        url: "/profile/:id",
        templateUrl: "templates/account/profile.html",
        controller: "ProfileCtrl"
      })
      .state('main.settings', {
        url: "/settings",
        templateUrl: "templates/main/settings.html",
        controller: "SettingsProfileCtrl",
        data: {
          authorization: true,
          redirectTo: 'login',
          memory: true
        }
      })
      .state('signin', {
        url: "/signin",
        templateUrl: "templates/account/signin.html",
        controller: "SigninCtrl"
      })
      .state('signup', {
        url: "/signup",
        templateUrl: "templates/account/signup.html",
        controller: "SignupCtrl",
        data: {
          redirectTo: 'signin'
        }
      })
     ;

  }]);
