angular.module('app')
  .run(['_', '$rootScope', '$state', 'Auth', 'Pessoa', function (_, $rootScope, $state, Auth, Pessoa) {
    $rootScope.limparMensagens = function(){
      $rootScope.closeAlertDanger();
      $rootScope.closeAlertInfo();
      $rootScope.closeAlertWarning();
      $rootScope.closeAlertSuccess();
    };

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
      if (Auth.memorizedState && Auth.memorizedState == toState.name) {
        Auth.memorizedState = undefined;
      }
      if (_.has(toState, 'data') && _.has(toState.data, 'authorization') && toState.data.authorization && !Auth.logged()) {
        if (_.has(toState.data, 'memory') && toState.data.memory) {
          Auth.memorizedState = toState.name;
        }
        if (_.has(toState.data, 'redirectTo')) {
          event.preventDefault();
          $state.go(toState.data.redirectTo);
        } else {
          $rootScope.alertDanger = 'Você precisa estar autenticado para acessar esta pagina';
          event.preventDefault();
        }

      } else if (_.has(toState, 'data') && _.has(toState.data, 'role') && toState.data.role.length > 0) {
        if(Auth.logged() && !Auth.getCurrentUser().nome){
          Pessoa.get(function (user) {
            if(toState.data.role.indexOf('admin') > -1 && !user.eh_admin){
              if(toState.data.role.indexOf('proprietario') > -1 && !user.eh_prop) {
                $rootScope.alertDanger = 'Você não tem permissão para acessar esta pagina';
                event.preventDefault();
                return ;
              }
            } else if(toState.data.role.indexOf('proprietario') > -1 && !user.eh_prop) {
              $rootScope.alertDanger = 'Você não tem permissão para acessar esta pagina';
              event.preventDefault();
              return ;
            }
            $rootScope.limparMensagens();
          }, function (err) {
            Auth.logout();
          });
        } else {
          if(toState.data.role.indexOf('admin') > -1 && !Auth.getCurrentUser().eh_admin){
            if(toState.data.role.indexOf('proprietario') > -1 && !Auth.getCurrentUser().eh_prop) {
              $rootScope.alertDanger = 'Você não tem permissão para acessar esta pagina';
              event.preventDefault();
              return ;
            }
          } else if(toState.data.role.indexOf('proprietario') > -1 && !Auth.getCurrentUser().eh_prop) {
            $rootScope.alertDanger = 'Você não tem permissão para acessar esta pagina';
            event.preventDefault();
            return ;
          }
          $rootScope.limparMensagens();
        }
      } else {
        $rootScope.limparMensagens();
      }
    });
  }]);
