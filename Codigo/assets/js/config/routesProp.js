angular.module('app')
  .config(['$stateProvider', function ($stateProvider) {

    $stateProvider
      .state('prop', {
        abstract: true,
        url: "/prop",
        templateUrl: "templates/prop/index.html"
      })
      .state('prop.home', {
        url: "",
        templateUrl: "templates/prop/dashboard.html",
        controller: "DashboardCtrl",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.evento', {
        url: "/Evento",
        templateUrl: "templates/prop/listarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.rascunho', {
        url: "/Evento/Rascunhos",
        templateUrl: "templates/prop/listarEventoRascunho.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.historico', {
        url: "/Evento/Historico",
        templateUrl: "templates/prop/listarEventoHistorico.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.criarEvento', {
        url: "/Evento/criarEvento",
        templateUrl: "templates/prop/criarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.editarEvento', {
        url: "/Evento/editarEvento/:id",
        templateUrl: "templates/prop/editarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.visualizarEvento', {
        url: "/visualizarEvento/:id",
        templateUrl: "templates/prop/visualizarEvento.html",
        controller: "EventoController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.agenda', {
        url: "/Agenda/:id",
        templateUrl: "templates/prop/listarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.criarAgenda', {
        url: "/Agenda/criarAgenda/:id",
        templateUrl: "templates/prop/criarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.editarAgenda', {
        url: "/Agenda/editarAgenda/:id/:idAgenda",
        templateUrl: "templates/prop/editarAgenda.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })
      .state('prop.AgendaHistorico', {
        url: "/Agenda/Historico/:id",
        templateUrl: "templates/prop/listarAgendaHistorico.html",
        controller: "AgendaController",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      })

      .state('prop.relatorioDeVendas', {
        url: "/relatorio/vendas",
        templateUrl: "templates/prop/relatorioVendas.html",
        controller: "RelatorioCtrl",
        data: {
          authorization: true,
          role: ['admin', 'proprietario'],
          redirectTo: 'signin',
          memory: true
        }
      });



  }]);

