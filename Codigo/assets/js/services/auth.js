angular.module('app')
    .factory('Auth', ['$location', '$http', '$cookies', '$q', 'Util', 'Pessoa', '$rootScope', function ($location, $http, $cookies, $q, Util, Pessoa, $rootScope) {

        var authService = {};
        var currentUser = {};

        authService.authService = undefined;

        if ($cookies.get('token')) {
            currentUser = Pessoa.get(function (user) {
                currentUser = user;
            }, function (err) {
                authService.logout();
            });
        }

        /**
         * Authenticate user and save token
         *
         * @param  {Object}   user     - login info
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        authService.login = function (user, callback) {
            $http.post('api/signin', {
                    login: user.login,
                    senha: user.senha
                })
                .then(function (res) {
                    $cookies.put('token', res.data.token);
                    localStorage.setItem('token', res.data.token);
                    currentUser = Pessoa.get();
                    return currentUser.$promise;
                })
                .then(function (user) {
                    $cookies.putObject('user', user);
                    Util.safeCb(callback)(null, user);
                    return user;
                })
                .catch(function (err) {
                    authService.logout();
                    Util.safeCb(callback)(err.data);
                    return $q.reject(err.data);
                });
        };

        /**
         * Delete access token and user info
         *
         * @param  {Function}
         */
        authService.logout = function () {
            $cookies.remove('token');
            currentUser = {};
            $rootScope.getUser();
        };

        /**
         * Create a new user
         *
         * @param  {Object}   user     - user info
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        authService.createUser = function (user, callback) {
          $http.post('api/signup', {
              nome: user.nome,
              login: user.login,
              senha: user.senha
            })
            .then(function (user) {
              $cookies.putObject('user', user);
              Util.safeCb(callback)(null, user);
              return user;
            })
            .catch(function (err) {
              authService.logout();
              Util.safeCb(callback)(err.data);
              return $q.reject(err.data);
            });
        };

        /**
         * Change password
         *
         * @param  {String}   oldPassword
         * @param  {String}   newPassword
         * @param  {Function} callback    - optional
         * @return {Promise}
         */
        authService.changePassword = function (oldPassword, newPassword, callback) {
            var cb = callback || angular.noop;

            return Pessoa.changePassword({id: currentUser.id}, {
                oldPassword: oldPassword,
                newPassword: newPassword
            }, function (user) {
                return cb(user);
            }, function (err) {
                return cb(err);
            }).$promise;
        };

        /**
         * Gets all available info on authenticated user
         *
         * @return {Object} user
         */
        authService.getCurrentUser = function () {
            return currentUser;
        };

        /**
         * Check if a user is logged in
         *
         * @return {Boolean}
         */
        authService.isLoggedIn = function () {
            return currentUser.hasOwnProperty('role');
        };

        /**
         * Waits for currentUser to resolve before checking if user is logged in
         */
        authService.isLoggedInAsync = function (cb) {
            if (currentUser.hasOwnProperty('$promise')) {
                currentUser.$promise.then(function () {
                    cb(true);
                }).catch(function () {
                    cb(false);
                });
            } else if (currentUser.hasOwnProperty('role')) {
                cb(true);
            } else {
                cb(false);
            }
        };

        /**
         * Check if a user is an admin
         *
         * @return {Boolean}
         */
        authService.isAdmin = function () {
            return currentUser.role === 'admin';
        };

        /**
         * Get auth token
         */
        authService.getToken = function () {
          var token =  $cookies.get('token');
            localStorage.setItem('token', token);
            return token;
        };

        authService.logged = function () {
            return authService.getToken() ? true : false;
        };

        return authService;
    }]);
