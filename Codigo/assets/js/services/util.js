angular.module('app')
      .factory('Util',['$window', '$filter', 'ngTableParams', function ($window, $filter, ngTableParams) {
        var Util = {};

        /**
         * Return a callback or noop function
         *
         * @param  {Function|*} cb - a 'potential' function
         * @return {Function}
         */
        Util.safeCb = function (cb) {
            return (angular.isFunction(cb)) ? cb : angular.noop;
        };

        /**
         * Parse a given url with the use of an anchor element
         *
         * @param  {String} url - the url to parse
         * @return {Object}     - the parsed url, anchor element
         */
        Util.urlParse = function (url) {
            var a = document.createElement('a');
            a.href = url;
            return a;
        };

        /**
         * Test whether or not a given url is same origin
         *
         * @param  {String}           url       - url to test
         * @param  {String|String[]}  [origins] - additional origins to test against
         * @return {Boolean}                    - true if url is same origin
         */
        Util.isSameOrigin = function (url, origins) {
            url = Util.urlParse(url);
            origins = (origins && [].concat(origins)) || [];
            origins = origins.map(Util.urlParse);
            origins.push($window.location);
            origins = origins.filter(function (o) {
                return url.hostname === o.hostname &&
                    url.port === o.port &&
                    url.protocol === o.protocol;
            });
            return (origins.length >= 1);
        };

        Util.ngTableParams = function(array, filter){
            var arrayFiltrated = $filter('filter')(array, filter);
            var tableParams =  new ngTableParams({
                page: 1,
                count: 10
            }, {
                counts: [],
                debugMode: false,
                total: array.length,
                getData: function($defer, params) {
                    var tableData = params.sorting() ? $filter('orderBy')(arrayFiltrated, params.orderBy()) : data;
                    tableData	= $filter('filter')(tableData, params.filter());
                    params.total(tableData.length);
                    $defer.resolve(tableData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

            return tableParams;
        };

        /**
         * Converts data uri to Blob. Necessary for uploading.
         * @see
         *   http://stackoverflow.com/questions/4998908/convert-data-uri-to-file-then-append-to-formdata
         * @param  {String} dataURI
         * @return {Blob}
         */
        Util.dataURItoBlob = function(dataURI) {
            console.log(dataURI);
            var binary = atob(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var array = [];
            for(var i = 0; i < binary.length; i++) {
                array.push(binary.charCodeAt(i));
            }
            return new Blob([new Uint8Array(array)], {type: mimeString});
        };

        return Util;
    }]);
