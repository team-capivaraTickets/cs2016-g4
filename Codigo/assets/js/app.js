'use strict';

/**
 * @ngdoc overview
 * @name appApp
 * @description
 * # appApp
 *
 * Main module of the application.
 */
angular.module('app', ['ngCookies', 'ngResource', 'ui.router', 'ngFileUpload', 'ngImgCrop', 'ngTable', 'angular-jwt', 'ui.mask', 'ngNotify']);
angular.module('app')
  .config(['$urlRouterProvider', '$locationProvider', function ($urlRouterProvider, $locationProvider) {

    $locationProvider.html5Mode(false);
    $urlRouterProvider.otherwise("/");

  }]);
