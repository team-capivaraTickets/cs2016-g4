angular.module('app')
  .constant('LAYOUT', {
    "menuMain": [
      {
        "active": false,
        "text": "Home",
        "link": "main.home",
        "dropdown": []
      }
    ],

    "menuAdmin": [
      {
        "active": false,
        "text": "Dashboard",
        "link": "admin.home",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Evento",
        "link": "admin.evento",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Cliente",
        "link": "admin.pessoa",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Pagamentos",
        "link": "admin.pagamento",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Relatório de Venda",
        "link": "admin.relatorioDeVendas",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Enviar Email",
        "link": "admin.enviarEmail",
        "dropdown": []
      }
    ],

    "menuProp": [
      {
        "active": false,
        "text": "Dashboard",
        "link": "prop.home",
        "dropdown": []
      },
      {
        "active": false,
        "text": "Evento",
        "link": "prop.evento",
        "dropdown": [

          {
            "active": false,
            "text": "Meus Eventos",
            "link": "prop.evento",
            "dropdown": []
          },
          {
            "active": false,
            "text": "Rascunho",
            "link": "prop.rascunho",
            "dropdown": []
          },
          {
            "active": false,
            "text": "Histórico de Eventos",
            "link": "prop.historico",
            "dropdown": []
          }


        ]
      },
      {
        "active": false,
        "text": "Relatório de Venda",
        "link": "prop.relatorioDeVendas",
        "dropdown": []
      }
    ]

  });
