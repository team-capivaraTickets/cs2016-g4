angular.module('app').directive('menu', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                menus: '='
            },
            link: function ($scope, $element) {

            },
            class: function (menu) {
                if(menu.dropdown.length > 0){
                    return 'dropdown';
                }

            },
            template: '' +
            '<ul class="nav navbar-nav">' +
                '<li ng-repeat="m in menus" ng-class="m.class">' +
                    '<a ng-if="m.dropdown.length == 0" ui-sref="{{m.link}}" ui-sref-active="active">{{m.text}}</a>' +
                    '<a ng-if="m.dropdown.length != 0" ui-sref-active="active" class="dropdown-toggle" data-toggle="dropdown" style="cursor: pointer;">{{m.text}} <span class="caret"></span></a>' +
                    '<ul ng-if="m.dropdown.length != 0" class="dropdown-menu" role="menu" style="cursor: pointer;">' +
                        '<li ng-repeat="subm in m.dropdown">' +
                            '<a ui-sref="{{subm.link}}" ui-sref-active="active">{{subm.text}}</a>' +
                          '</li>' +
                    '</ul>' +
                '</li>' +
            '</ul>'
        };
    });
