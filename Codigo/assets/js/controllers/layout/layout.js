angular.module('app')
  .controller('LayoutCtrl', ['$scope', '$rootScope', '$location', '$cookies', 'LAYOUT', 'Pessoa', 'Auth', '$http', function ($scope, $rootScope, $location, $cookies, LAYOUT, User, Auth, $http) {

    $rootScope.closeAlertDanger = function () {
      $rootScope.alertDanger = undefined;
    };

    $rootScope.closeAlertInfo = function () {
      $rootScope.alertInfo = undefined;
    };

    $rootScope.closeAlertWarning = function () {
      $rootScope.alertWarning = undefined;
    };

    $rootScope.closeAlertSuccess = function () {
      $rootScope.alertSuccess = undefined;
    };

    $rootScope.getUser = function () {
      $rootScope.user = Auth.getCurrentUser();
      $rootScope.token = Auth.getToken();
    };

    $scope.switchRoute = function () {

      //Populate menu
      $scope.menu = LAYOUT.menuMain;
      if ($location.url().indexOf('admin') > -1) {
        $scope.menu = LAYOUT.menuAdmin;
      } else if ($location.url().indexOf('prop') > -1) {
        $scope.menu = LAYOUT.menuProp;
      }

    };

    $scope.logoutUser = function () {
      Auth.logout();
      $scope.getUser();
      $location.path('/');
    };

    $rootScope.getUser();
    $scope.switchRoute();
    $scope.$on('$locationChangeSuccess', $scope.switchRoute);
    $scope.$on('LoginUser', $rootScope.getUser);
    $scope.$on('LogoutUser', $scope.logoutUser);
  }]);

