/**
 * Created by Ana Elisa on 03/03/2016.
 */
angular.module('app').controller('ClienteController', ['$scope','$http', '$stateParams', 'ngNotify', '$state', function($scope, $http, $stateParams, ngNotify, $state){

  $scope.idEvento = $stateParams.id;
  $scope.idAgenda = $stateParams.id;
  $scope.buscarEvento = function(){

    var query = "SELECT * FROM evento as e, endereco as en WHERE";
    var estado;
    var dataInicio;
    var dataFinal;
    var cidade;
    var tipoCategoria;

    if($scope.estado != null)
    {
      if( query.length != 46) {
        query = query + ' AND';
      }
      estado = " en.estado = " + $scope.estado;
      query = query + estado;
    }
    if($scope.dataInicio != null)
    {
      if( query.length != 46) {
        query = query + ' AND';
      }
      dataInicio = " e.dataInicio = " + $scope.dataInicio;
      query = query + dataInicio;
    }
    if($scope.dataFinal != null)
    {
      if( query.length != 46) {
        query = query + ' AND';
      }
      dataFinal = " e.dataFinal = " + $scope.dataFinal;
      query = query + dataFinal;
    }
    if($scope.cidade != null)
    {
      if( query.length != 46) {
        query = query + ' AND';
      }
      cidade = " en.cidade = " + $scope.cidade;
      query = query + cidade;
    }
    if($scope.tipoCategoria != null)
    {
      if( query.length != 46) {
        query = query + ' AND';
      }
      tipoCategoria = " e.dataFinal = " + $scope.tipoCategoria;
      query = query + tipoCategoria;
    }
    $http.get('api/Evento/findByQuery/' + query).then(function(evento){

      $scope.listEvento = evento.data.rows;
      console.log($scope.listEvento);

    },function(err){
      console.log(err);
    });

  };

  $scope.listarEvento = function(){
    $http.get('api/Evento/list').then(function(evento){

      console.log('Evento.data: ' + evento.data.rows);
      $scope.listEvento = evento.data.rows;
      console.log($scope.listEvento);

    },function(err){
      console.log(err);
    });
  };
  $scope.listarEventoCresc = function(){

  };
  $scope.buscarAgenda = function(){

  };
  $scope.listar = function(id){

    $http.get('api/Evento/findById/' + id).then(function(evento){

      $scope.listEvento = evento.data.rows;
      console.log($scope.listEvento);

    },function(err){
      console.log(err);
    });

    var query = "SELECT * FROM agenda WHERE evento_id = " + id + "titulo = " + $scope.titulo;

    $http.get('api/Agenda/findByQuery/' + query).then(function(agenda){

      $scope.listAgenda = agenda.data.rows;
      console.log($scope.listAgenda);

    },function(err){
      console.log(err);
    });

  };

}]);
