/**
 * Created by Ana Elisa on 03/03/2016.
 */
angular.module('app').controller('CarrinhoController', ['$scope', '$http', '$stateParams', '$rootScope', 'ngNotify', '$state', '$timeout', function ($scope, $http, $stateParams, $rootScope, ngNotify, $state, $timeout) {

  $scope.idEvento = $stateParams.idEvento;
  $scope.idAgenda = $stateParams.idAgenda;
  $scope.pagamentoId = $stateParams.idPagamento;
  $scope.idCarrinho = $rootScope.carrinho.id;

  console.log('minha agenda' + $scope.idAgenda);

  $scope.dataVencimento = function (data) {
    var d = new Date(data);
    d.setDate(d.getDate() + 3);

    var data = new Date(d.getFullYear(), d.getMonth() + 1, d.getDate());
    return data;
  };

  /*
   * CRIAR CARRINHO VAZIO
   * */
  $scope.criarCarrinho = function () {

    var carrinho = {
      valor_total: 0,
      pessoa: $scope.pessoa_id,
      desconto: $scope.desconto,
      status: false
    };
    $scope.valorTotal = 0;
    $http.post('api/Carrinho/create', carrinho).then(function (carrinho) {

    }, function (err) {

    });

  };

  /*
   * EFETUAR COMPRA: essa função apenas agrega o pagamento com o Carrinho, a compra é realmente efetuada quando o propietario
   * altera o atributo status_pagamento de false para true
   * */

  $scope.efetuarCompra = function (idCarrinho) {

    var objPagamento = {
      tipo_pagamento: $scope.tipoPagamento,
      data_compra: $scope.dataCompra,
      data_vencimento: $scope.dataVencimento($scope.dataCompra),
      status_pagamento: false,
      carrinho: idCarrinho
    };
    $http.post('api/Pagamento/create', objPagamento).then(function (pagamento) {

    }, function (err) {

    });


  };

  /*
   * BUSCA carrinho pelo ID
   * */

  $scope.buscarCarrinho = function () {

    $http.get('api/Carrinho/findById/' + $scope.idCarrinho).then(function (carrinho) {

      $scope.listaCarrinho = carrinho.data;

    }, function (err) {
      ngNotify.set('Não foi possível encontrar um carrinho!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

    $http.get('api/Ingresso/findByIdCarrinho/' + $scope.idCarrinho).then(function (ingressos) {

      $scope.listaIngressosCarrinho = ingressos.data;

      console.log('valor ' + $scope.listaIngressosCarrinho.length);
      console.log($scope.listaIngressosCarrinho[2]);

      for(var i = 0; i < $scope.listaIngressosCarrinho.length; i++) {

        //select * from agenda as ag, evento as ev where ag.evento_id = ev.id;
        var query = "SELECT * FROM agenda AS ag, evento AS ev, ingresso AS ing WHERE ag.id = " + $scope.listaIngressosCarrinho[i].agenda + " AND ag.evento_id = ev.id AND ing.carrinho_id = " + $scope.idCarrinho;
        $http.get('api/Agenda/findByQuery/' + query).then(function (agenda) {

          $scope.listaBusca = agenda.data.rows;
          console.log($scope.listaBusca);

        }, function (err) {
        });
      }

    }, function (err) {
      ngNotify.set('Não foi possível encontrar ingressos do carrinho!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

  };

  /*
   * FAZ UPDATE DE INGRESSO NO BANCO DE DADOS
   * */

  $scope.editarIngresso = function (objIngresso) {

    $http.put('api/Ingresso/update', objIngresso).then(function (ingresso) {

      ngNotify.set('Edição de ingresso realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });


    }, function (err) {
      //alert('Não foi possível editar Ingresso');

      ngNotify.set('Não foi possível editar um ingresso!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });

  };

  /*
   * Altera qualquer TABELA E ATRIBUTO DO BANCO DE DADOS
   * */

  $scope.updateByQuery = function (tabela, id, coluna, valor) {
    var t = "carrinho";
    var query = "UPDATE " + t + " SET " + coluna + " = " + valor + " WHERE id = " + id;


    $http.post('api/' + tabela + '/updateByQuery', query).then(function (Tabela) {
      ngNotify.set('Pedido encaminhado para pagamento pendente.', {
        type: 'warning',
        position: 'top',
        duration: 3000
      });

    }, function (err) {
      ngNotify.set('Não foi possível encaminhar pedido para pagamento.', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

  };

  $scope.finalizarCompra = function (id) {
    $scope.updateByQuery("Carrinho", id, "status", true);
    $scope.efetuarCompra(id);
  };

  /*
   * GERENCIAR PAGAMENTO
   *
   * Propietario altera status de pagamento
   *
   * */

  /*
   * Traz do Banco de dados para Front-End Pagamanto
   * */
  $scope.editar = function () {

    $http.get('api/Pagamento/findById/' + $scope.pagamentoId).then(function (pagamento) {

      var Pagamento = pagamento.data[0];

      $scope.tipo_pagamento = Pagamento.tipoPagamento;
      $scope.data_compra = Pagamento.dataCompra;
      $scope.carrinho = Pagamento.carrinho;
      $scope.id = Pagamento.id;

      //Pagamento.status_pagamento = (Pagamento.status_pagamento == true ? "Pago" : "Não Pago");

      $scope.s = {
        status: Pagamento.status_pagamento,
        availableOptions: [
          {boolean: true, status_pagamento: 'Pago'},
          {boolean: false, status_pagamento: 'Não Pago'}
        ]
      };


    }, function (err) {
    });

  };


  $scope.editarPagamento = function () {

    var objPagamento = {
      id: $scope.id,
      tipo_pagamento: $scope.tipoPagamento,
      //data_compra: $scope.dataCompra,
      data_pagamento: $scope.data_pagamento,
      data_vencimento: $scope.dataV,
      status_pagamento: $scope.s.status,
      carrinho: $scope.carrinho
    };

    $http.put('api/Pagamento/update', objPagamento).then(function (ingresso) {

      ngNotify.set('Edição de Pagamento realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });

    }, function (err) {
      ngNotify.set('Não foi possível editar um pagamento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

    $http.get('api/Carrinho/findById/' + $scope.carrinho).then(function (carrinho) {

      $scope.listaCarrinho = carrinho.data[0];

      for (var i = 0; i < $scope.listaCarrinho.ingressos.length; i++) {
        if ($scope.listaCarrinho.ingressos[i].tipo == 'meia') {
          $scope.listaCarrinho.ingressos[i].valor = $scope.listaCarrinho.ingressos[i].valor / 2;
        }
        var objI = {
          id: $scope.listaCarrinho.ingressos[i].id,
          numero: $scope.listaCarrinho.ingressos[i].numero,
          tipo: $scope.listaCarrinho.ingressos[i].tipo,
          valor: $scope.listaCarrinho.ingressos[i].valor,
          status: 'ocupado',
          assento_preferencial: $scope.listaCarrinho.ingressos[i].assento_preferencial,
          posicao_coluna: $scope.listaCarrinho.ingressos[i].posicao_coluna,
          posicao_fileira: $scope.listaCarrinho.ingressos[i].posicao_fileira
        };
        $scope.editarIngresso(objI);

      }


    }, function (err) {
    });
    $state.go('prop.Pagamentos');
    $scope.listarPagamentos();

  };

  $scope.listarPagamentos = function () {

    //listar Pagamento
    var coluna = "p.id, p.data_vencimento, p.data_pagamento, p.status_pagamento, p.tipo_pagamento, p.carrinho, h.nome";
    var where = "p.carrinho = c.id AND c.pessoa_id = h.id";
    var query = "SELECT " + coluna + " FROM pagamento as p, carrinho as c, pessoa as h WHERE " + where;

    $http.get('api/Pagamento/list/' + query).then(function (pagamento) {

      $scope.listPagamento = pagamento.data.rows;

    }, function (err) {
    });

  };

  $scope.cancelarCompra = function (idCarrinho, idPagamento) {

    $http.delete('api/Pagamento/destroy/' + idPagamento).then(function (evento) {

    }, function (err) {

    });

    $http.get('api/Carrinho/findById/' + $scope.carrinho).then(function (carrinho) {

      $scope.listaCarrinho = carrinho.data[0];

      for (var i = 0; i < $scope.listaCarrinho.ingressos.length; i++) {
        if ($scope.listaCarrinho.ingressos[i].tipo == 'meia') {
          $scope.listaCarrinho.ingressos[i].valor = $scope.listaCarrinho.ingressos[i].valor * 2;
        }
        var objI = {
          id: $scope.listaCarrinho.ingressos[i].id,
          numero: $scope.listaCarrinho.ingressos[i].numero,
          tipo: 'inteira',
          valor: $scope.listaCarrinho.ingressos[i].valor,
          status: 'livre',
          assento_preferencial: $scope.listaCarrinho.ingressos[i].assento_preferencial,
          posicao_coluna: $scope.listaCarrinho.ingressos[i].posicao_coluna,
          posicao_fileira: $scope.listaCarrinho.ingressos[i].posicao_fileira
        };
        $scope.editarIngresso(objI);

      }

    }, function (err) {
    });


    $http.delete('api/Carrinho/destroy/' + id).then(function (carrinho) {

      ngNotify.set('Exclusão de carrinho realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });

    }, function (err) {

      ngNotify.set('Não foi possível excluir um carrinho!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });
  };

  $scope.listarIngressosAgenda = function () {
    $http.get('api/Agenda/listIngresso/' + $scope.idAgenda).then(function (ingresso) {

      $scope.listaIngresso = ingresso.data.ingressos;

    }, function (err) {
      ngNotify.set('Não foi possível encontrar a lista de ingressos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });
  };

  $scope.salvarIngressoNoCarrinho = function (ingresso) {
    ingresso.status = 'reservado';

    var objIngresso = {
      id: $rootScope.carrinho.id,
      obj: ingresso
    };
    $scope.valorTotal = $scope.valorTotal + ingresso.valor;

    $http.post('api/Carrinho/createIngresso', objIngresso).then(function (ingresso) {

      $http.get('api/Agenda/findById/' + $scope.idAgenda).then(function (agenda) {
        $scope.minhaAgenda = agenda.data[0];

        var objAgenda = {
          id: $scope.minhaAgenda.id,
          data: $scope.minhaAgenda.data,
          titulo: $scope.minhaAgenda.titulo,
          horario_inicio: $scope.minhaAgenda.horario_inicio,
          horario_final: $scope.minhaAgenda.horario_final,
          qtde: $scope.minhaAgenda.qtde - 1,
          qtd_fileiras: $scope.minhaAgenda.qtd_fileiras,
          qtd_colunas: $scope.minhaAgenda.qtd_colunas,
          valor_ingresso: $scope.minhaAgenda.valor_ingresso,
          evento_id: $scope.minhaAgenda.evento_id
        };

        $http.put('api/Agenda/update', objAgenda).then(function (agenda) {
        });

      });

      ngNotify.set('Ingresso adicionado ao carrinho com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });
    }, function (err) {
      ngNotify.set('Não foi possível adicionar ingresso ao carrinho para você!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });
  };

  $scope.salvarIngressoCarrinhoTh = function (ingresso) {

    if (!$rootScope.carrinho) {
      $http.get('api/Carrinho/getCarrinho').then(function (carrinho) {
        $rootScope.carrinho = carrinho.data;
        $scope.idCarrinho = $rootScope.carrinho.id;
        $scope.salvarIngressoNoCarrinho(ingresso);
      }, function (err) {
        ngNotify.set('Não foi possível encontrar um carrinho para você!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
      });
    } else {
      $scope.salvarIngressoNoCarrinho(ingresso);
    }

  };

  $scope.valorIngresoMeiaOuInteria = function (index) {
    if ($scope.listaIngresso[index].tipo == 'meia') {
      $scope.listaIngresso[index].valor = $scope.listaIngresso[index].valor / 2;
    } else if ($scope.listaIngresso[index].tipo == 'inteira') {
      $scope.listaIngresso[index].valor = $scope.listaIngresso[index].valor * 2;
    }
  };

  $scope.removerIngressoCarrinho = function (ingresso) {
    if (confirm('Deseja remover esse ingresso do carrinho?')) {
      ingresso.carrinho_id = '0';
      ingresso.status = 'livre';

      $http.put('api/Ingresso/update', ingresso).then(function (carrinho) {

        var idAgendaTemp = carrinho.data[0];

        $scope.buscarCarrinho();

        $http.get('api/Agenda/findById/' + idAgendaTemp.agenda).then(function (agenda) {
          $scope.minhaAgenda = agenda.data[0];

          var objAgendaNova = {
            id: $scope.minhaAgenda.id,
            data: $scope.minhaAgenda.data,
            titulo: $scope.minhaAgenda.titulo,
            horario_inicio: $scope.minhaAgenda.horario_inicio,
            horario_final: $scope.minhaAgenda.horario_final,
            qtde: $scope.minhaAgenda.qtde + 1,
            qtd_fileiras: $scope.minhaAgenda.qtd_fileiras,
            qtd_colunas: $scope.minhaAgenda.qtd_colunas,
            valor_ingresso: $scope.minhaAgenda.valor_ingresso,
            evento_id: $scope.minhaAgenda.evento_id
          };

          $http.put('api/Agenda/update', objAgendaNova).then(function (agenda) {
          });

        });

        ngNotify.set('Ingresso removido do carrinho com sucesso!', {
          type: 'success',
          position: 'top',
          duration: 3000
        });
      }, function (err) {
        ngNotify.set('Não foi possível remover o ingresso do carrinho para você!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
      });
    }
  };

}]);
