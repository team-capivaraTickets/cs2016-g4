angular.module('app')
  .controller('SettingsCtrl', ['$scope', '$rootScope', '$http', 'ngNotify', function ($scope, $rootScope, $http, ngNotify) {

    $scope.listEventoEmail = [];

    $scope.listarEvento = function () {
      $http.get('api/Evento/listarTudo').then(function (evento) {
        $scope.listEvento = evento.data;
      }, function (err) {
        console.log(err);
      });
    };

    $scope.addInEmail = function (evento) {
      $scope.listEventoEmail.push(evento);
    };

    $scope.sendEmail = function () {
      var send = {
        eventos: $scope.listEventoEmail
      };

      $http.post('api/Email/sendEmail', send).then(function (result) {

        ngNotify.set('Email de aviso enviado com sucesso para todos usuarios cadastrados.', {
          type: 'success',
          position: 'top',
          duration: 3000
        });
      }, function (err) {
        ngNotify.set('Não foi possível enviar o aviso por email!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
      });
    };
  }]);
