/**
 * Created by Ana Elisa on 01/03/2016.
 */
angular.module('app').controller('IngressoController', ['$scope', '$http', '$stateParams', 'ngNotify', '$state', function ($scope, $http, $stateParams, ngNotify, $state) {

  console.log('$stateParams.idIngresso: ' + $stateParams.idIngresso);
  $scope.idAgenda = $stateParams.idAgenda;
  $scope.idIngresso = $stateParams.idIngresso;

  $scope.listarIngresso = function () {
    console.log(' $scope.idAgenda: ' + $scope.idAgenda);

    $http.get('api/Agenda/listIngresso/' + $scope.idAgenda).then(function (ingresso) {

      console.log('ingresso.length: ' + ingresso.data.ingressos.length);
      $scope.listIngresso = ingresso.data.ingressos;
      console.log($scope.listIngresso);

    }, function (err) {
      console.log(err);
      ngNotify.set('Não foi possível encontrar a lista de ingressos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });
  };

  $scope.editar = function () {

    $http.get('api/Ingresso/findById/' + $scope.idIngresso).then(function (ingresso) {


      var Ingresso = ingresso.data[0];
      $scope.numero = Ingresso.numero;
      $scope.posicao_coluna = Ingresso.posicao_coluna;
      $scope.posicao_fileira = Ingresso.posicao_fileira;

      $scope.t = {
        T: Ingresso.tipo,
        availableOptions: [
          {tipo: 'inteira'},
          {tipo: 'meia'}
        ]
      };
      $scope.s = {
        S: Ingresso.status,
        availableOptions: [
          {status: 'livre'},
          {status: 'ocupado'},
          {status: 'reservado'}
        ]
      };
      var label = (Ingresso.assento_preferencial ? "sim" : "Não");
      console.log('Ingresso.assento_preferencial: ' + Ingresso.assento_preferencial);

      $scope.aP = {
        AP: label,
        availableOptions: [
          {boolean: true, assento_preferencial: 'Sim'},
          {boolean: false, assento_preferencial: 'Não'}
        ]
      };
      $scope.valor = Ingresso.valor;

    }, function (err) {
      ngNotify.set('Não foi possível encontrar um ingresso!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

  };

  $scope.editarIngresso = function () {

    console.log(' $scope.Tipo: ' + $scope.t.repeatSelectgenda);
    if ($scope.t.T == 'meia') {
      $scope.valor = ($scope.valor / 2);
    }
    var objIngresso = {
      id: $scope.idIngresso,
      numero: $scope.numero,
      tipo: $scope.t.T,
      valor: $scope.valor,
      status: $scope.s.S,
      assento_preferencial: $scope.aP.AP,
      posicao_coluna: $scope.posicao_coluna,
      posicao_fileira: $scope.posicao_fileira
    };
    $http.put('api/Ingresso/update', objIngresso).then(function (ingresso) {


      console.log(ingresso);
      ngNotify.set('Edição de ingresso realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });


    }, function (err) {
      ngNotify.set('Não foi possível editar um ingresso!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });

  };

}]);
