angular.module('app').controller('AgendaController', ['$scope', '$http', '$stateParams', 'ngNotify', '$state', '$rootScope', function ($scope, $http, $stateParams, ngNotify, $state, $rootScope) {

  //console.log('$stateParams.id: ' + $stateParams.id);
  //console.log('$stateParams.idAgenda: ' + $stateParams.idAgenda);
  $scope.idEvento = $stateParams.id;
  $scope.idAgenda = $stateParams.idAgenda;

  $scope.newDateFormat = function (data) {
    var I = new Date(data);

    var i = I.getFullYear();
    if ((I.getMonth() + 1) < 10) {
      i = i + '-0' + (I.getMonth() + 1);
    }
    else {
      i = i + '-' + (I.getMonth() + 1);
    }
    if (I.getDate() < 10) {
      i = i + '-0' + I.getDate();
    }
    else {
      i = i + '-' + I.getDate();
    }
    return i;
    //console.log($scope.dataI);
  };

  $scope.dia = function(){


    var query = "SELECT * FROM evento WHERE id  = " +  $scope.idEvento;
    $http.get('api/Evento/findByQuery/' + query).then(function (evento) {

      $scope.e = evento.data.rows[0];
      $scope.dataMin = $scope.newDateFormat($scope.e.dataInicio);
      console.log('dataMin: ' + $scope.dataMin);
      $scope.dataMax = $scope.newDateFormat($scope.e.dataFinal);
      console.log('dataMax: ' + $scope.dataMax);



    }, function (err) {
      console.log('Não foi possivel ...');
      /*ngNotify.set('Não foi possível encontrar um evento!', {
       type: 'error',
       position: 'top',
       duration: 3000
       });*/
    });


  };


  $scope.criarAgenda = function () {

    if (!$scope.form.$valid) {
      ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
      return;
    }

    if(isNaN($scope.rascunho)){
      $scope.rascunho = false;
    }
    console.log('Id Evento: ' + $stateParams.id);
    var objAgenda = {
      data: $scope.date,
      titulo: $scope.titulo,
      horario_inicio: $scope.horario_inicio,
      horario_final: $scope.horario_final,
      qtde: ($scope.qtdFileiras * $scope.qtdColunas),
      qtd_fileiras: $scope.qtdFileiras,
      qtd_colunas: $scope.qtdColunas,
      valor_ingresso: $scope.valorIngresso,
      evento_id: $scope.idEvento,
      rascunho: $scope.rascunho
    };


    console.log(objAgenda);
    //persistir Agenda no Banco de Dados
    $http.post('api/Agenda/create', objAgenda).then(function (objAgendaRespose) {



      var agenda = objAgendaRespose.data.agenda;

      console.log(agenda);
      if(agenda.rascunho == false){
        agenda.ingressos = $scope.gerarIngresso(agenda.qtd_fileiras, agenda.qtd_colunas, agenda.valor_ingresso, agenda.id);

        $http.put('api/Agenda/update/'+agenda.id, agenda).then(function (agenda) {
          ngNotify.set('Cadastro de Ingressos na agenda realizado com sucesso.', {
            type: 'success',
            position: 'top',
            duration: 3000
          });
          //$scope.listarAgenda();
        }, function (err) {
          ngNotify.set('Não foi possível cadastrar os ingressos a sua agenda!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });

      }
      ngNotify.set('Cadastro de agenda realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });

    }, function (err) {
      ngNotify.set('Não foi possível cadastrar uma agenda!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });


    //limpando campos do formulário.
    $scope.date = "";
    $scope.titulo = "";
    $scope.horario_inicio = "";
    $scope.horario_final = "";
    $scope.qtde = "";
    $scope.qtdFileiras = "";
    $scope.qtdColunas = "";
    $scope.valorIngresso = "";
    $scope.rascunho = "";

    $state.go('prop.agenda', {id: $scope.idEvento});
    $scope.listarAgenda();
  };

  $scope.listarAgenda = function () {

    console.log(' $scope.idEvento: ' + $scope.idEvento);
    //listar Agenda

    $http.get('api/Agenda/list/' + $scope.idEvento).then(function (agenda) {

      //console.log('agenda.data: ' + agenda.data);
      $scope.listAgenda = agenda.data;
      //console.log($scope.listAgenda);

    }, function (err) {
      console.log(err);
    });
  };


  $scope.editar = function () {

    $http.get('api/Agenda/findById/' + $scope.idAgenda).then(function (agenda) {


      var Agenda = agenda.data[0];

      //console.log('Agenda.data' + Agenda.data);
      $scope.date = $scope.newDateFormat(Agenda.data);
      //console.log($scope.date);
      $scope.titulo = Agenda.titulo;
      $scope.horario_inicio = new Date(Agenda.horario_inicio);
      $scope.horario_final = new Date(Agenda.horario_final);
      $scope.qtde = Agenda.qtde;
      $scope.qtdFileiras = Agenda.qtd_fileiras;
      $scope.qtdColunas = Agenda.qtd_colunas;
      $scope.valorIngresso = Agenda.valor_ingresso;
      $scope.rascunho = Agenda.rascunho;


    }, function (err) {
      console.log(err);
    });

  };

  $scope.editarAgenda = function () {

    if (!$scope.form.$valid) {
      ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
      return;
    }

    if(isNaN($scope.rascunho)){
      $scope.rascunho = false;
    }

    var objAgenda = {
      id: $scope.idAgenda,
      data: $scope.date,
      titulo: $scope.titulo,
      horario_inicio: $scope.horario_inicio,
      horario_final: $scope.horario_final,
      qtde: $scope.qtde,
      qtd_fileiras: $scope.qtdFileiras,
      qtd_colunas: $scope.qtdColunas,
      valor_ingresso: $scope.valorIngresso,
      evento_id: $scope.idEvento,
      rascunho: $scope.rascunho
    };


    //console.log('Editar: ' + objAgenda);
    //persistir Evento no Banco de Dados

    $http.put('api/Agenda/update', objAgenda).then(function (agenda) {

      var Agenda = agenda.data[0];
      console.log('Agenda Update Ingresso ...');
      console.log(Agenda);
      console.log(Agenda.id);

      if(Agenda.rascunho == false)
      {
        Agenda.ingressos = $scope.gerarIngresso(Agenda.qtd_fileiras, Agenda.qtd_colunas, Agenda.valor_ingresso, Agenda.id);

        console.log('Ingresso ...');
        console.log(Agenda.ingressos);

        $http.put('api/Agenda/update/'+ Agenda.id, Agenda).then(function (agenda) {
          ngNotify.set('Cadastro de Ingresso realizado com sucesso.', {
            type: 'success',
            position: 'top',
            duration: 3000
          });
          //$scope.listarAgenda();
        }, function (err) {
          ngNotify.set('Não foi possível cadastrar os ingressos a sua agenda!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });
      }

      //console.log(agenda);
      ngNotify.set('Edição de agenda realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });


    }, function (err) {
      ngNotify.set('Não foi possível editar uma agenda!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });

    //limpando campos do formulário.
    $scope.date = "";
    $scope.titulo = "";
    $scope.horario_inicio = "";
    $scope.horario_final = "";
    $scope.qtde = "";
    $scope.qtdFileiras = "";
    $scope.qtdColunas = "";
    $scope.valorIngresso = "";
    $scope.rascunho = "";

    $state.go('prop.agenda', {id: $scope.idEvento});
    $scope.listarAgenda();

  };

  $scope.excluirAgenda = function (id) {

    if (confirm('Deseja excluir Agenda?')) {

      console.log('Excluir id: ' + id);

      $http.delete('api/Agenda/destroy/' + id).then(function (agenda) {

        ngNotify.set('Exclusão de agenda realizado com sucesso.', {
          type: 'success',
          position: 'top',
          duration: 3000
        });

        $scope.listarAgenda();

      }, function (err) {
        ngNotify.set('Não foi possível excluir uma agenda!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });

      });
    }
    // $scope.$apply();


  };
  $scope.buscarAgendabyId = function () {


    $http.get('api/Agenda/findById/' + $scope.idAgenda).then(function (agenda) {

      $scope.listBusca = agenda.data[0];
      //console.log($scope.listBusca);

    }, function (err) {
      console.log(err);
    });

  };
  $scope.buscarAgenda = function (valores) {


    $http.get('api/Agenda/find/' + valores).then(function (agenda) {

      $scope.listBusca = agenda.data[0];
      //console.log($scope.listBusca);

    }, function (err) {
      console.log(err);
    });

  };
  $scope.gerarIngresso = function (qtdFileira, qtdColuna, valor, id) {

    var objIngresso = [];
    var count = 1;
    for (var i = 1; i <= qtdColuna; i++) {
      for (var j = 1; j <= qtdFileira; j++) {
        obj = {
          numero: count,
          tipo: 'inteira',
          valor: valor,
          status: 'livre',
          assento_preferencial: false,
          posicao_coluna: i,
          posicao_fileira: j,
          agenda: id
        };
        //console.log('OBJINGRESSO: ' + obj);
        objIngresso.push(obj);

        count++;
      }
    }
    return objIngresso;
  };

  $scope.listarEventoComAgenda = function () {

    //console.log(' $scope.idEvento: ' + $scope.idEvento);
    //listar Agenda

    $http.get('api/Evento/findById/' + $scope.idEvento).then(function (evento) {

      $scope.listBusca = evento.data;
      //console.log('ListBusca');
      //console.log($scope.listBusca);

    }, function (err) {
      ngNotify.set('Não foi possível encontrar um evento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

    $http.get('api/Agenda/listTudo/' + $scope.idEvento).then(function (agenda) {

      //console.log('agenda.data: ' + agenda.data);
      $scope.listAgenda = agenda.data;
      //console.log($scope.listAgenda);

    }, function (err) {
      console.log(err);
      ngNotify.set('Não foi possível encontrar uma agenda!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });
  };

}]);
