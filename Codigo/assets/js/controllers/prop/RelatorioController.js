/**
 * Created by Jeferson Urbieta on 09/03/2016.
 */
angular.module('app')
  .controller('RelatorioCtrl', ['$scope', '$rootScope', '$http', 'ngNotify', function ($scope, $rootScope, $http, ngNotify) {
    $scope.buscarDadosIniciais = function () {
      if($rootScope.user.eh_admin){
        $http.get('api/Proprietario/query').then(function (props) {
          $scope.proprietarios = props.data;
        }, function (err) {
          console.log(err);
        });
      } else {
        $http.get('api/Proprietario/find/'+$rootScope.user.id).then(function (props) {
          $scope.proprietarios = [];
          $scope.proprietarios.push(props.data);
        }, function (err) {
          console.log(err);
        });
      }
    };

    $scope.pesquisar = function () {

      if (!$scope.form.$valid || !$scope.relatorio.tipo) {
        ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
        return;
      }

      var param = {
        relatorio: $scope.relatorio
      };
      $http.post('api/Relatorio/relatorioVendas', param).then(function (relatorio) {
        $scope.relatorios = relatorio.data;
        console.log($scope.relatorios);
        ngNotify.set('Pesquisa realizada com sucesso.', {
          type: 'success',
          position: 'top',
          duration: 3000
        });

      }, function (err) {
        ngNotify.set('Não foi possível realizar a busca!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
      });

    };

  }]);
