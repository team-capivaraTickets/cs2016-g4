angular.module('app').controller('EventoController', ['$scope', '$http', '$stateParams', '$rootScope', 'ngNotify', '$state', 'Upload', function ($scope, $http, $stateParams, $rootScope, ngNotify, $state, Upload) {


  $scope.listEvento = [];
  $scope.listEnd = [];
  $scope.eventoId = $stateParams.id;
  $scope.pessoaId = $rootScope.user.id;

  // console.log('$rootScope... ' + $scope.pessoaId);


  $scope.newDateFormat = function () {
    var I = new Date();

    var i = I.getFullYear();
    if ((I.getMonth() + 1) < 10) {
      i = i + '-0' + (I.getMonth() + 1);
    }
    else {
      i = i + '-' + (I.getMonth() + 1);
    }
    if (I.getDate() < 10) {
      i = i + '-0' + I.getDate();
    }
    else {
      i = i + '-' + I.getDate();
    }
    return i;
    //console.log($scope.dataI);
  };

  $scope.dataI = $scope.newDateFormat();
  $scope.dataF = $scope.newDateFormat($scope.dataI);

  $scope.dataFormat = function (data) {
    var I = new Date(data);

    var i = I.getFullYear();
    if ((I.getMonth() + 1) < 10) {
      i = i + '-0' + (I.getMonth() + 1);
    }
    else {
      i = i + '-' + (I.getMonth() + 1);
    }
    if (I.getDate() < 10) {
      i = i + '-0' + I.getDate();
    }
    else {
      i = i + '-' + I.getDate();
    }
    return i;
  };

  /*
   *
   * Criar Evento
   *
   * */

  $scope.criarEvento = function () {

    if (!$scope.form.$valid) {
      ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
      return;
    }

    var objEndereco = {
      estado: $scope.estado,
      cidade: $scope.cidade,
      rua: $scope.rua,
      numero: $scope.numero,
      complemento: $scope.complemento,
      bairro: $scope.bairro,
      cep: $scope.cep
    };

    if (isNaN($scope.rascunho)) {
      $scope.rascunho = false;
    }

    //console.log('$scope.rascunho: ' + $scope.rascunho);


    var objEvento = {
      nome: $scope.nome,
      imagem: null,
      descricao: $scope.descricao,
      dataInicio: $scope.dataInicio,
      dataFinal: $scope.dataFinal,
      qtdIngressoTotal: $scope.qtdIngressoTotal,
      tipoCategoria: $scope.tipoCategoria,
      count_views: 0,
      endereco: objEndereco,
      pessoa_id: $scope.pessoaId,
      rascunho: $scope.rascunho
    };

    //persistir Evento no Banco de Dados
    $http.post('api/Evento/create', objEvento).then(function (objEvento) {

      if ($scope.file) {
        $scope.upload($scope.file, objEvento.data.id);
      }
      $scope.listarEvento();

      ngNotify.set('Cadastro de evento realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });

      //limpando campos do formulário.
      $scope.nome = "";
      $scope.descricao = "";
      $scope.dataInicio = "";
      $scope.dataFinal = "";
      $scope.tipoCategoria = "";
      $scope.qtdIngressoTotal = "";
      $scope.estado = "";
      $scope.cidade = "";
      $scope.rua = "";
      $scope.numero = "";
      $scope.complemento = "";
      $scope.bairro = "";
      $scope.cep = "";
      $scope.rascunho = "";


      $state.go('prop.evento');

    }, function (err) {
      ngNotify.set('Não foi possível cadastrar um evento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });

  };

  /*
   * Upload files
   * */
  $scope.upload = function (file, eventoId) {
    Upload.upload({
      url: 'api/Evento/uploadImage/' + eventoId,
      data: {
        file: file
      }
    }).then(function (resp) {
      //console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
    }, function (resp) {
      //console.log('Error status: ' + resp.status);
    });
  };

  /*
   *
   * Listar Evento
   *
   * */

  $scope.listarEvento = function () {

    //listar Evento
    //console.log('Evento...');
    $http.get('api/Evento/list/' + $scope.pessoaId).then(function (evento) {

      //console.log(evento.data);
      $scope.listEvento = evento.data;
      //console.log($scope.listEvento);

    }, function (err) {
      console.log(err);
    });

  };

  /*
   *
   * Editar
   *
   * */

  $scope.editar = function () {
    //trazendo Evento e Endereço para o frontEnd

    $http.get('api/Evento/findById/' + $scope.eventoId).then(function (evento) {

      var Evento = evento.data[0];
      //console.log('OBjeto Evento.endereco.cidade: ' + Evento.endereco.cidade);

      var dI = new Date(Evento.dataInicio);
      //console.log('OBjeto Evento.endereco.cidade: ' + Evento.endereco.cidade);

      $scope.nome = Evento.nome;
      $scope.descricao = Evento.descricao;
      $scope.dataInicio = new Date(Evento.dataInicio);
      $scope.dataFinal = new Date(Evento.dataFinal);
      $scope.tipoCategoria = Evento.tipoCategoria;
      $scope.qtdIngressoTotal = Evento.qtdIngressoTotal;

      $scope.estado = Evento.endereco.estado;
      $scope.cidade = Evento.endereco.cidade;
      $scope.rua = Evento.endereco.rua;
      $scope.numero = Evento.endereco.numero;
      $scope.complemento = Evento.endereco.complemento;
      $scope.bairro = Evento.endereco.bairro;
      $scope.cep = Evento.endereco.cep;
      $scope.idEnd = Evento.endereco.id;
      $scope.count_views = Evento.count_views;
      $scope.rascunho = Evento.rascunho;


    }, function (err) {
    });

  };

  /*
   *
   * Editar Evento
   *
   * */

  $scope.editarEvento = function () {

    if (!$scope.form.$valid) {
      ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
      return;
    }

    var objEndereco = {
      id: $scope.idEnd,
      estado: $scope.estado,
      cidade: $scope.cidade,
      rua: $scope.rua,
      numero: $scope.numero,
      complemento: $scope.complemento,
      bairro: $scope.bairro,
      cep: $scope.cep
    };

    var objEvento = {
      id: $scope.eventoId,
      nome: $scope.nome,
      descricao: $scope.descricao,
      dataInicio: $scope.dataInicio,
      dataFinal: $scope.dataFinal,
      qtdIngressoTotal: $scope.qtdIngressoTotal,
      tipoCategoria: $scope.tipoCategoria,
      count_views: $scope.count_views,
      endereco: objEndereco,
      pessoa_id: $scope.pessoaId,
      rascunho: $scope.rascunho


    };

    //console.log('Editar: ' + objEvento);
    //persistir Evento no Banco de Dados
    //console.log(JSON.stringify(objEvento));
    $http.put('api/Evento/update', objEvento).then(function (evento) {

      if ($scope.file) {
        $scope.upload($scope.file, evento.data[0].id);
      }
      console.log(evento);
      ngNotify.set('Edição de evento realizado com sucesso.', {
        type: 'success',
        position: 'top',
        duration: 3000
      });


    }, function (err) {
      ngNotify.set('Não foi possível editar um evento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });

    });

    //limpando campos do formulário.
    $scope.nome = "";
    $scope.descricao = "";
    $scope.dataInicio = "";
    $scope.dataFinal = "";
    $scope.tipoCategoria = "";
    $scope.qtdIngressoTotal = "";
    $scope.estado = "";
    $scope.cidade = "";
    $scope.rua = "";
    $scope.numero = "";
    $scope.complemento = "";
    $scope.bairro = "";
    $scope.cep = "";
    $scope.rascunho = "";

    $scope.listarEvento();

    $state.go('prop.evento');

  };

  /*
   *
   * Excluir Evento
   *
   * */

  $scope.excluirEvento = function (id) {

    if (confirm('Deseja excluir Evento?')) {

      //console.log('Exluir id: ' + id);

      $http.delete('api/Evento/destroy/' + id).then(function (evento) {

        ngNotify.set('Exclusão de evento realizado com sucesso.', {
          type: 'success',
          position: 'top',
          duration: 3000
        });
        $scope.listarEvento();

      }, function (err) {
        ngNotify.set('Não foi possível excluir um evento!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });

      });
    }
  };

  $scope.buscarEventobyId = function () {


    $http.get('api/Evento/findById/' + $scope.eventoId).then(function (evento) {

      $scope.listBusca = evento.data[0];
      //console.log('ListBusca');
      //console.log($scope.listBusca);

    }, function (err) {
      ngNotify.set('Não foi possível encontrar um evento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

  };
  $scope.buscarEvento = function (variavel, valor) {

    var query = "SELECT * FROM evento WHERE " + variavel + " =  " + valor;
    $http.get('api/Evento/findByQuery/' + query).then(function (evento) {

      $scope.listBusca = evento.data.rows;
      console.log($scope.listBusca);

    }, function (err) {
      ngNotify.set('Não foi possível encontrar um evento!', {
        type: 'error',
        position: 'top',
        duration: 3000
      });
    });

  };

  /*
   *
   * Listar Evento
   *
   * */

  $scope.listarTodosEventos = function () {

    $http.get('api/Evento/listarTudo/').then(function (evento) {
      $scope.listaEvento = evento.data;
      console.log('Estou aqui');
      console.log($scope.listaEvento);
    });

  };

  /*
   * Atualiza a quantidade de visualizações do Evento
   *  */

  $scope.updateCountViews = function (idEvento) {

    var count;
    var query = "SELECT * FROM evento WHERE id = " + idEvento;
    $http.get('api/Evento/findByQuery/' + query).then(function (evento) {

      count = evento.data.rows[0];

      //console.log('count antes ' + count.count_views);

      count.count_views = count.count_views + 1;

      //console.log('count depois ' + count.count_views);

      var query = "UPDATE evento SET count_views = " + count.count_views + " WHERE id = " + idEvento;

      //console.log(query);
      $http.put('api/Evento/updateByQuery', {"id": query}).then(function (Evento) {

      }, function (err) {

      });


    }, function (err) {
    });

  };

  $scope.listarEventoRascunho = function () {

    var query = "SELECT * FROM evento as e, endereco as en WHERE rascunho = true AND e.endereco = en.id AND pessoa_id = " + $scope.pessoaId;

    $http.get('api/Evento/findByQuery/' + query).then(function (evento) {

      //console.log(evento.data);
      $scope.listEvento = evento.data.rows;
      //console.log($scope.listEvento);

    }, function (err) {
      console.log(err);
    });

  };
  $scope.listarEventoHistorico = function () {

    var i = $scope.newDateFormat(new Date());
    console.log('i: ' + i);
    var string = '"dataFinal"';
    var query = "SELECT * FROM evento as e, endereco as en WHERE " + string + " < '" + i + "' AND e.endereco = en.id AND pessoa_id = " + $scope.pessoaId;
    console.log('query: ' + query);

    $http.get('api/Evento/findByQuery/' + query).then(function (evento) {

      //console.log(evento.data);
      $scope.listEvento = evento.data.rows;
      //console.log($scope.listEvento);

    }, function (err) {
      console.log(err);
    });

  };


}]);
