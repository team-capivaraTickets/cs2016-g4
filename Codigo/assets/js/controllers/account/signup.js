angular.module('app')
  .controller('SignupCtrl', ['$scope', '$rootScope', '$state', 'Auth', 'ngNotify', function ($scope, $rootScope, $state, Auth, ngNotify) {
    $scope.user = {};

    $scope.register = function (form) {
      $scope.submitted = true;

      if (form.$valid) {
        Auth.createUser({
          nome: $scope.user.nome,
          login: $scope.user.login,
          senha: $scope.user.senha
        }, function (err, usuario) {
          if (err) {
            ngNotify.set(err, {
              type: 'error',
              position: 'top',
              duration: 3000
            });
          }
          $state.go('signin');
        });
      }
    };
  }]);
