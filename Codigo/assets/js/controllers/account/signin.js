angular.module('app')
  .controller('SigninCtrl', ['$scope', '$location', '$window', '$state', 'Auth', 'ngNotify', function ($scope, $location, $window, $state, Auth, ngNotify) {
    $scope.user = {};

    $scope.login = function (form) {
      $scope.submitted = true;
      if (form.$valid) {
        Auth.login({
          login: $scope.user.login,
          senha: $scope.user.senha
        }, function (err, user) {
          if (err) {
            if(err.code == 'E_USER_NOT_FOUND'){
              $scope.errors = 'Usuario não encontrado. Tente novamente com um usuario cadastrado.';
            } else if(err.code == 'E_WRONG_PASSWORD'){
              $scope.errors = 'Senha incorreta. Tente novamente com a senha certa.';
            }
            ngNotify.set($scope.errors, {
              type: 'error',
              position: 'top',
              duration: 3000
            });
          } else {
            $scope.$emit('LoginUser');
            if (Auth.memorizedState) {
              $state.go(Auth.memorizedState);
            } else {
              $state.go('main.home');
            }
          }
        });
      }
    };
  }]);
