angular.module('app')
    .controller('ProfileCtrl', ['$scope', 'Auth', 'Util', '$http', '$stateParams', 'ngNotify', '$compile', function ($scope, Auth, Util, $http, $stateParams, ngNotify, $compile) {

      $scope.searchData = function(list){
        $scope.tableParams = Util.ngTableParams(list, $scope.search);
      };

      $scope.getTableParams = function(){
        return $scope.tableParams;
      };

      $scope.listarMeusPedidos = function(){
        $http.get('api/Evento/meusPedidos').then(function(evento){
          $scope.eventos = evento.data;
        },function(err){
          console.log(err);
        });
      };

      $scope.listarHistoricoCompra = function(){
        $http.get('api/Pagamento/historicoPagamento').then(function(histPag){
          $scope.historicoCompra = histPag.data;
        },function(err){
          console.log(err);
        });
      };

      $scope.buscarHistoricoCompra = function(){

        $http.get('api/Pagamento/detalheHistoricoPagamento/'+$stateParams.id).then(function(pag){
          $scope.pagamento = pag.data;
        },function(err){
          console.log(err);
        });
      };

      $scope.exibirDadosPessoais = function(){

      };

      $scope.salvarDadosPessoais = function(){

      };

      $scope.trocarSenha = function(){
        if (!$scope.form.$valid) {
          ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
          return;
        }

        console.log($scope.senhaAntiga);
        $http.post('api/changePassword', {
          oldPassword: $scope.senhaAntiga,
          newPassword: $scope.senhaNova
        }).then(function (response) {
          ngNotify.set('Alteração de senha realizado com sucesso.', {
            type: 'success',
            position: 'top',
            duration: 3000
          });
        }, function (erro) {
          console.log(erro);
          if(erro.data.code == 'E_UNAUTHORIZED'){
            $scope.messageError = 'Não foi possível alterar senha, porque a senha digitada esta incorreta.';
          } else {
            $scope.messageError = 'Não foi possível alterar senha!';
          }
          ngNotify.set($scope.messageError, {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });
      };

    $scope.idPessoa = $stateParams.idPessoa;

    console.log('id pessoa meu cartao ' + $scope.idPessoa);

    $scope.salvarCartao = function () {

    };

    }]);
