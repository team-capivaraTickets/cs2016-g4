/**
 * Created by troliveira on 27/02/2016.
 */

angular.module('app')
  .controller('PessoaCtrl', ['$scope', '$rootScope', '$http', '$stateParams', 'ngNotify', '$state', function ($scope, $rootScope, $http, $stateParams, ngNotify, $state) {

    $scope.buscarPessoa = function () {
      if ($state.current.name == 'main.minhaConta.dadosPessoais') {
        $scope.id = $rootScope.user.id;
        $scope.stateDadosPessoais = true;
      } else {
        $scope.id = $stateParams.id;
      }
      console.log($scope.id);

      var pessoaTemp = {};
      var endTemp = {};

      // Encontra a pessoa
      $http.get('api/Pessoa/find/' + $scope.id).then(function (response) {
        $scope.pessoa = response.data;
        pessoaTemp = response.data;

        // Encontra o endereço
        $http.get('api/Endereco/find/' + pessoaTemp.endereco_id).then(function (response) {
          endTemp = response.data;

          $scope.rua = endTemp.rua;
          $scope.complemento = endTemp.complemento;
          $scope.bairro = endTemp.bairro;
          $scope.numero = endTemp.numero;
          $scope.cep = endTemp.cep;
          $scope.cidade = endTemp.cidade;
          $scope.estado = endTemp.estado;

        }, function (erro) {
        });
      }, function (erro) {
        ngNotify.set('Não foi possível encontrar um cliente!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
      });


        // Encontra o proprietário
        $http.get('api/Proprietario/find/' + $scope.id).then(function (response) {
          $scope.prop = response.data;
        }, function (erro) {
          ngNotify.set('Não foi possível encontrar informações da empresa!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });

    };

    /**
     * Excultado quando entra na tela
     * Verifica se tem que buscar pessoa ou se é uma nova pessoa
     */
    if ($stateParams.id || $state.current.name == 'main.minhaConta.dadosPessoais') {
      $scope.buscarPessoa();
    } else {
      $scope.pessoa = {};
      $scope.proprietario = {};
      $scope.prop = {};
    }

    $scope.salvarPessoa = function () {

      if (!$scope.form.$valid) {
        ngNotify.set('Há campos inválidos no seu formulário, preencha-os com valores válidos!', {
          type: 'error',
          position: 'top',
          duration: 3000
        });
        return;
      }

      var endereco = {
        rua: $scope.rua,
        numero: $scope.numero,
        bairro: $scope.bairro,
        cep: $scope.cep,
        complemento: $scope.complemento,
        cidade: $scope.cidade,
        estado: $scope.estado
      };

      if ($scope.pessoa.id) {

        // Atualiza os dados do endereço
        $http.put('api/Endereco/update/' + $scope.pessoa.endereco_id, endereco).then(function (response) {
        }, function (erro) {
        });

        if ($scope.pessoa.eh_prop) {
          // Atualiza os dados do proprietário
          $http.put('api/Proprietario/update/' + $scope.pessoa.id, $scope.prop).then(function (response) {
          }, function (erro) {
          });
        }

        // Atualiza os dados da pessoa
        $http.put('api/Pessoa/update/' + $scope.pessoa.id, $scope.pessoa).then(function (response) {
          ngNotify.set('Edição de cliente realizado com sucesso.', {
            type: 'success',
            position: 'top',
            duration: 3000
          });

          if ($scope.stateDadosPessoais) {
            $state.go('main.minhaConta.dadosPessoais');
          } else {
            $state.go('main.pessoa');
          }
        }, function (erro) {

          var erroMessage = '';
          if (erro.data.message.indexOf('`cpf` already exists') > -1) {
            erroMessage = 'Não foi possivel editar um cliente. CPF já cadastrado!';
          } else if (erro.data.message.indexOf('`email` already exists') > -1) {
            erroMessage = 'Não foi possivel editar um cliente. E-mail já cadastrado!';
          } else if (erro.data.message.indexOf('"email" validation rule failed for input') > -1) {
            erroMessage = 'Não foi possivel editar um cliente. E-mail informado é inválido!';
          } else if (erro.data.message.indexOf('`nome` already exists') > -1) {
            erroMessage = 'Não foi possivel editar um cliente. Nome já cadastrado!';
          } else if (erro.data.message.indexOf('`login` already exists') > -1) {
            erroMessage = 'Não foi possivel editar um cliente. Login já cadastrado!';
          } else {
            erroMessage = 'Não foi possível editar um cliente!';
          }

          ngNotify.set(erroMessage, {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });

      } else {

        var empresa = {
          pessoa_id: '',
          razao_social: $scope.prop.razao_social,
          nome_fantasia: $scope.prop.nome_fantasia,
          cnpj: $scope.prop.cnpj,
          site_empresa: $scope.prop.site_empresa
        };

        var pessoaTeste = {
          nome: $scope.pessoa.nome,
          cpf: $scope.pessoa.cpf,
          email: $scope.pessoa.email,
          login: $scope.pessoa.login,
          senha: $scope.pessoa.senha,
          telefone: $scope.pessoa.telefone,
          eh_prop: $scope.pessoa.eh_prop,
          eh_deficiente: $scope.pessoa.eh_deficiente,
          endereco_id: ''
        };

        // Salva antes o endereço da pessoa
        $http.post('api/Endereco/create', endereco).then(function (response) {
          pessoaTeste.endereco_id = response.data.id;

          console.log(pessoaTeste.endereco_id);

          // Salva a pessoa
          $http.post('api/Pessoa/create', pessoaTeste).then(function (response) {
            empresa.pessoa_id = response.data.id;

            if (pessoaTeste.eh_prop) {
              // Por fim salva os dados do proprietário

              $http.post('api/Proprietario/create', empresa).then(function (response) {
                ngNotify.set('Cadastro de proprietário realizado com sucesso.', {
                  type: 'success',
                  position: 'top',
                  duration: 3000
                });
              }, function (erro) {
              });
            } else {
              ngNotify.set('Cadastro de cliente realizado com sucesso.', {
                type: 'success',
                position: 'top',
                duration: 3000
              });
            }

            $state.go('signin');

          }, function (erro) {
            var erroMessage = '';
            if (erro.data.message.indexOf('`cpf` already exists') > -1) {
              erroMessage = 'Não foi possivel cadastrar um cliente. CPF já cadastrado!';
            } else if (erro.data.message.indexOf('`email` already exists') > -1) {
              erroMessage = 'Não foi possivel cadastrar um cliente. E-mail já cadastrado!';
            } else if (erro.data.message.indexOf('`nome` already exists') > -1) {
              erroMessage = 'Não foi possivel cadastrar um cliente. Nome já cadastrado!';
            } else if (erro.data.message.indexOf('"email" validation rule failed for input') > -1) {
              erroMessage = 'Não foi possivel cadastrar um clinete. E-mail informado é inválido!';
            } else if (erro.data.message.indexOf('`login` already exists') > -1) {
              erroMessage = 'Não foi possivel cadastrar um cliente. Login já cadastrado!';
            }

            ngNotify.set(erroMessage, {
              type: 'error',
              position: 'top',
              duration: 3000
            });
          });

        }, function (erro) {
          ngNotify.set('Não foi possível cadastrar um endereço!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });
      }

    };

    $scope.listarPessoa = function () {
      // Lista todos os dados da pessoa com endereço e proprietário
      $http.get('api/Pessoa/findAll').then(function (response) {
        $scope.items = response.data;
      }, function (erro) {
      });

      // Lista todos os dados da pessoa com endereço
      $http.get('api/Pessoa/query').then(function (response) {
        $scope.itens = response.data;
      }, function (erro) {
      });
    };

    $scope.apagarPessoa = function (item) {

      if (confirm('Deseja excluir esse cliente?')) {

        // Exclusão do endereço
        $http.delete('api/Endereco/destroy/' + item.endereco_id, {params: {id: item.endereco_id}}).then(function (response) {
        }, function (erro) {
        });

        if (item.eh_prop) {
          var idEvento = '';

          // Exclusão do proprietário
          $http.delete('api/Proprietario/destroy/' + item.id, {params: {id: item.id}}).then(function (response) {
          }, function (erro) {
          });

          $http.get('api/Evento/findByIdPessoa/' + item.id, {params: {id: item.id}}).then(function (response) {
            idEvento = response.data.id;

            $http.delete('api/Evento/destroy/' + idEvento, {params: {id: idEvento}}).then(function (response) {
              console.log(response);
            }, function (erro) {
              console.log(erro);
            });
          }, function (erro) {
            console.log(erro);
          });

        }

        // Por fim exclusão da pessoa
        $http.delete('api/Pessoa/destroy/' + item.id, {params: {id: item.id}}).then(function (response) {
          $scope.itens.splice($scope.itens.indexOf(item, 1));
          ngNotify.set('Exclusão de cliente realizado com sucesso.', {
            type: 'success',
            position: 'top',
            duration: 3000
          });
          $scope.listarPessoa();
        }, function (erro) {
          ngNotify.set('Não foi possível excluir uma pessoa!', {
            type: 'error',
            position: 'top',
            duration: 3000
          });
        });

      }
    };


  }]);
