/**
 * isAuthenticated
 * @description :: Policy that inject user in `req` via JSON Web Token
 */

var passport = require('passport');

module.exports = function (req, res, next) {
  passport.authenticate('jwt', function (error, pessoa, info) {
    if (error) {
      return res.serverError(error);
    }
    if (!pessoa) {
      return res.unauthorized(null, info && info.code, info && info.message);
    }

    req.pessoa = pessoa;

    next();
  })(req, res);
};
