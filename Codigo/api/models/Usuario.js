/**
 * Usuario
 * @description :: Model for storing Usuario
 */

module.exports = {

  attributes: {

    id: {
      type: 'integer',
      unique: true,
      primaryKey: true,
      autoIncrement: true
    },

    nome: {
      type: 'string',
      required: true
    },

    senha: {
      type: 'string',
      required: true
    },

    login: {
      type: 'string',
      required: true,
      unique: true
    },

    toJSON: function () {
      var obj = this.toObject();
      delete obj.senha;
      return obj;
    }
  },

  beforeUpdate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  },

  beforeCreate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  }
};
