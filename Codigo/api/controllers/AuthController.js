/**
 * AuthController
 * @description :: Server-side logic for manage user's authorization
 */
var passport = require('passport');


module.exports = {
  /**
   * Register usuario no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signup: function (req, res) {
    Usuario
      .create(_.omit(req.allParams(), 'id'))
      .then(function (usuario) {
        return {
          token: CipherService.createToken(usuario),
          usuario: usuario
        };
      })
      .then(res.created)
      .catch(res.serverError);
  },

  /**
   * Login pela estratégia local no passport
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signin: function (req, res) {
    passport.authenticate('local', function (error, usuario, info) {
      if (error)
        return res.serverError(error);

      if (!usuario)
        return res.unauthorized(null, info && info.code, info && info.message);

      return res.ok({
        token: CipherService.createToken(usuario),
        usuario: usuario
      });
    })(req, res);
  },

  /**
   * Retorna dados da Pessoa logada ao frontend
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  me: function (req, res) {
    return res.ok(req.usuario);
  },
  /**
   * Accept JSON Web Token and updates with new one
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  refresh_token: function (req, res) {
    res.badRequest(null, null, 'Not implemented yet');
  },

  /**
   * @description Logout de um Usuario no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  logout: function (req, res) {
    req.logout();
  },

  changePassword: function (req, res) {
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);

    Usuario.findOne({
      id: req.usuario.id
    }).exec(function (err, usuario) {
      if (err)
        return res.serverError(err);

      if (!usuario)
        return res.notFound();

      if (CipherService.comparePassword(oldPass, usuario)) {
        usuario.senha = newPass;
        Usuario.update(usuario.id, usuario, function (erro, sucesso) {
          if (erro)
            return res.serverError(erro);

          res.ok(sucesso);

        });
      } else {
        return res.unauthorized();
      }
    });
  }

};
