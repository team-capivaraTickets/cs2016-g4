/**
 * PessoaController
 * @description :: Server-side logic for manage users
 */

module.exports = {

  /**
   * Retorna dados da Pessoa logada ao frontend
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  me: function (req, res) {
    return res.ok(req.pessoa);
  },

  /**
   * Registrar pessoa no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  create: function (req, res) {

    var params = req.body;

    Pessoa
      .create(params, function (erro, sucesso) {
        if (erro) {
          return res.serverError(erro);
        } else {
          res.json(sucesso);
        }
      });
  },

  /**
   * Encontrar pessoa no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  find: function (req, res) {

    var id = req.param('id');

    Pessoa
      .findOne({
        id: id
      }).exec(function (err, finn) {
        if (err) {
          return res.negotiate(err);
        }
        if (!finn) {
          return res.notFound('Could not find Finn, sorry.');
        }

        sails.log('Found "%s"', finn.cpf);
        return res.ok(finn);
      });

  },

  /**
   * Alterar pessoa no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  update: function (req, res) {

    var id = req.param('id');

    if (!id) {
      return res.badRequest('Sem id informado.');
    }

    Pessoa
      .update(id, req.body, function (erro, sucesso) {

        if (sucesso.length === 0) {
          return res.notFound();
        }

        if (erro) {
          return next(erro);
        }

        res.json(sucesso);

      });
  },

  /**
   * Apagar pessoa no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  destroy: function (req, res) {

    var id = req.param('id');

    if (!id) {
      return res.badRequest('Sem id informado.');
    }

    Pessoa
      .find(id, function (erro, resultado) {

        if (erro) {
          return res.serverError(erro);
        }

        if (!resultado) {
          return res.notFound();
        }

        Pessoa
          .destroy(id, function (erro) {

            if (erro) {
              return next(erro);
            }

            return res.json(resultado);

          });

      });
  },

  /**
   * Listar pessoa, endereço e proprietário no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  findAll: function (req, res) {
    Proprietario.find().populate('pessoa_id').exec(function (err, evento) {
      if (err) {
        return res.serverError(err);
      }
      return res.ok(evento);
    });
  },

  /**
   * Listar pessoa com o endereço no sistema
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  query: function (req, res) {

    Pessoa.find().populate('endereco_id').exec(function (err, evento) {
      if (err) {
        return res.serverError(err);
      }
      return res.ok(evento);
    });
  }

};
