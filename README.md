# README #

### Introdução ###

Esta aplicação consiste no Trabalho Prático da disciplina de Construção de Software II da Universidade Federal de Mato Grosso do Sul.

### Tecnologias Usadas ###

####Back-end####
* Node.JS
* Sails
* Express  
* Grunt

####Front-end####
* Bower  
* AngularJS  
* Bootstrap 

####Banco de dados####
* Postgres

### Desenvolvedores ###

* Ana Elisa
* Jeferson
* Thalita